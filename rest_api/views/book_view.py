import json

from rest_framework import viewsets, generics, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from rest_api.models import Book, BookCategory, BookDetail
from rest_api.serializers import BookCategorySerializer, BookDetailSerializer, BookSerializer, ListBookSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    def list(self, request):
        query_params = request.query_params
        limit = 10
        page = 1

        if 'limit' in query_params and query_params['limit'][0].isdigit():
            limit = int(query_params['limit'][0])
        if 'page' in query_params and query_params['page'][0].isdigit():
            page = int(query_params['page'][0])

        start = (page - 1) * limit
        end = start + limit

        queryset = Book.objects.all()[start:end]
        count = Book.objects.count()
        serializer = ListBookSerializer(queryset, many=True)
        return Response({'data': serializer.data, 'count': count, 'limit': limit, 'page': page})


class BookCategoryViewSet(viewsets.ModelViewSet):
    queryset = BookCategory.objects.all()
    serializer_class = BookCategorySerializer


class BookDetailViewSet(viewsets.ModelViewSet):
    queryset = BookDetail.objects.all()
    serializer_class = BookDetailSerializer
