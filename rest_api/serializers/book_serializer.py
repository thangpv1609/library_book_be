from rest_framework import serializers

from rest_api.models import Book, BookDetail, BookCategory


class BookCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BookCategory
        fields = ['id', 'name', 'description']


class BookSerializer(serializers.ModelSerializer):
    book_category = serializers.PrimaryKeyRelatedField(queryset=BookCategory.objects.all())

    class Meta:
        model = Book
        fields = ['id', 'code', 'name', 'description', 'book_category']


class ListBookSerializer(serializers.ModelSerializer):
    book_category = BookCategorySerializer(read_only=True)

    class Meta:
        model = Book
        fields = ['id', 'code', 'name', 'description', 'book_category']


class BookDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BookDetail
        fields = ['id', 'code', 'book']
