from django.db import models


class BookCategory(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)


class Book(models.Model):
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    book_category = models.ForeignKey(BookCategory, on_delete=models.CASCADE)


class BookDetail(models.Model):
    code = models.CharField(max_length=200)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

